<?php
    ob_start();

    // include("lib/openCon.php");
    // include("lib/functions.php");

    require_once ('lib/class.phpmailer.php');

    function curlRequests($url, $params) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        return $json_string = json_decode($response, true);
    }
        
    $str = "";
    
    if (empty($_REQUEST['asso_nom']) || empty($_REQUEST['address']) || empty($_REQUEST['email']) || empty($_REQUEST['phone']) || empty($_REQUEST['nom']) ) {
        
        $str = '<div class="msg_box msg_error">Veuillez saisir les champs obligatoires, et vérifier le format de vos données.</div>';
    }
    else{
        try {

            // MAIL TO CONTACT@MYSYNA>COM
            $subject = "MySyna : Contact Association";
            $headers = "From: contact@oprotect.com\r\n";
            $headers.= "MIME-Version: 1.0\r\n";
            $headers.= "Content-Type: text/html; charset=UTF-8\r\n";
            $message = "<br />Nom de la synagogue ou de l’association: " . $_REQUEST['asso_nom'];
            $message.= "<br />Adresse: " . $_REQUEST['address'];
            $message.= "<br />Mail: " . $_REQUEST['email'];
            $message.= "<br />Nom : " . $_REQUEST['nom'];
            $message.= "<br />Téléphone: " . $_REQUEST['phone'];

            $mail = new PHPMailer(true); 
            
            // MESSAGE
            $mail->Subject = $subject;
            $mail->AltBody = $message; 
            $mail->WordWrap = 80; 
            $mail->MsgHTML($message);
            $mail->IsHTML(true);

            // ADDRESS
            $mail->From = "contact@mysyna.com";
            $mail->FromName = "MySyna";
            $to = "contact@mysyna.com";
            $mail->AddAddress($to);
            
            // SMTP SETTINGS
            $mail->IsSMTP();
            $mail->SMTPAuth = true;
            $mail->Port = 465; 
            $mail->SMTPSecure = 'ssl';
            $mail->Host = "smtp.zoho.com";
            $mail->Username = "danny@mysyna.com";
            $mail->Password = "Sarah2209";
            $mail->CharSet = "UTF-8";

            // SETUP
            $mail->Priority = 1;

            if (!$mail->Send()) {
                
                $str = '<div class="msg_box msg_ok">Une erreure est survenue. Veuillez réessayer plus tard.</div>';
            }
            else{
                
                $str = '<div class="msg_box msg_ok">Merci de votre inscription. Un mail de confirmation vous à été envoyé.</div>';
            }


            // MAIL TO CUSTOMER
            $ml = new PHPMailer(true);

            // MESSAGE
            $msg = '<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" style="font-family:Arial, sans-serif; max-width:600px;">
                    <tr><td align="center" style="padding:20px 0px 12px 0px;"><img src="http://www.mysyna.com/images/logo.png" alt="" style="max-width:70%;"></td></tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="10">
                                <tr><td style=" color:#2F2F2F; font-size:18px; "><strong>Bonjour et bienvenue sur MySyna !</strong> </td></tr>
                                <tr><td style=" color:#2F2F2F; font-size:16px; ">Vous venez de rejoindre MySyna et nous sommes ravis de vous compter parmi nous.  Nous vous rappellerons bientôt pour compléter les informations de votre communauté et finaliser la création de votre compte.
                                </td></tr>
                                <tr><td style=" color:#2F2F2F; font-size:16px; ">En attendant, nous vous invitons à visiter notre site www.mysyna.com afin d\'en savoir plus.<br/>
                                    Pour toute information, vous pouvez aussi nous contacter : <br/>
                                        Par téléphone au 01 83 80 92 58 du dimanche au vendredi<br/>
                                        Par mail à contact@mysyna.com<br/>
                                </td></tr>
                                <tr><td align="center"><a href="http://www.mysyna.com" style="background-color:#2F9EC4; padding:15px 0px; display:block; margin:auto; width:100%; font-size:18px; color:#FFF; text-align:center; text-decoration:none;" ><strong>ACCES AU SITE</strong></a></td></tr>
                                <tr><td style=" color:#2F2F2F; text-align:left; font-size:16px;">
                                    <table width="100%">
                                        <tr><td colspan="3" style="color:#2F2F2F; text-align:left; font-size:16px;"><strong>À bientôt sur notre site !</strong></td></tr>
                                        <tr><td colspan="3">&nbsp;</td></tr>
                                        <tr><td colspan="3">L\'équipe MySyna</td></tr>
                                    </table>
                                </td></tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td height="20"></td></tr>
                </table>';
                
            $ml->Subject = $subject;
            $ml->AltBody = $msg;
            $ml->WordWrap = 80;
            $ml->MsgHTML($msg);
            $ml->IsHTML(true);

            // ADDRESS
            $ml->From = "contact@mysyna.com";
            $ml->FromName = "MySyna";
            $ml->AddAddress($_REQUEST['email']);

            // SMTP SETTINGS
            $ml->IsSMTP(); 
            $ml->SMTPAuth = true; 
            $ml->Port = 465; 
            $ml->SMTPSecure = 'ssl';
            $ml->Host = "smtp.zoho.com";
            $ml->Username = "danny@mysyna.com";
            $ml->Password = "Sarah2209";
            $ml->CharSet = "UTF-8";

            // EMAIL SETTINGS
            $ml->Priority = 1;

            $ml->Send();
        }
        catch(phpmailerException $e){
            $str = '<div class="msg_box msg_error">>Une erreure est survenue. Veuillez réessayer plus tard.</div>';
        }
    }

    print ($str);

    ob_end_flush();
?>
